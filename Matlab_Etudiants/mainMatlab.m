% --- Clear all stuff 
clearvars; 
close all;
clc; 
% addpath('./FEC');
% addpath('./Radio/');
% addpath('./ASCII/');



% ---------------------------------------------------- 
%% --- Physical layer parameters  
% ---------------------------------------------------- 
% --- We should define here all the necessary parameters 
% --- L1 (top) parameters 
carrierFreq = 930e6;
bandwidth   = 8e6;
rxGain      = 25;

% utile pour synchronisation
nFFT = 1024;
nRe = 600;
Pb = 3; %dB
upss = 2;

% --- L1 (bottom) parameters




% ---------------------------------------------------- 
%% --- Iterative radio comm and processing 
% ----------------------------------------------------  
% --- Define flag modes for system 
% How we obtain the data 
%	- 'load'  : Loading a file 
%	- 'radio' : Getting data from USRP
modeAcqusition	  = 'load';	
bufferSize		  = 64000;



nbAcquisition = 1; 
for iAc = 1 : 1 : nbAcquisition 
	% ---------------------------------------------------- 
	%% ---  Buffer acqusition 
	% ---------------------------------------------------- 
	% --- We should have an input buffer where we will look 
	% after the usefull data and be ready to process it 
	if strcmp(modeAcqusition,'load')
		% ---------------------------------------------------- 
		%% --- Loading file 
		% ---------------------------------------------------- 
		% --- We load the file 
        name = 'signal_chan.mat';
		sigRx	= load(name);
	else 
		% ---------------------------------------------------- 
		%% --- Radio mode 
		% ---------------------------------------------------- 
		if iac == 1 
			% --- Parameter for radio 
			obj = pyUDPsocket(6789,'');
			e310_setConfigRadio('192.168.10.11','Rx','freq',carrierFreq);
			e310_setConfigRadio('192.168.10.11','Rx','samp_rate',bandwidth);
			e310_setConfigRadio('192.168.10.11','Rx','gain',rxGain);
		end 
		% --- Getting buffer from radio 
		sigRx = getDATA_UDP(obj,tgSize);
	end
	% ---------------------------------------------------- 
	%% --- Data processing  
	% ---------------------------------------------------- 
	% --- Synchronisation 
	% We should isolate one frame from all buffer to be processed 
	% We should estimate the delay and compensate the CFO
    if (name == 'signal_chan.mat')
        sigFramePSCH = synchronisation(sigRx.sigChan, nFFT, nRe, Pb, upss);
    else
        sigFramePSCH = synchronisation(sigRx.sigId, nFFT, nRe, Pb, upss);
    end
	% ---------------------------------------------------- 
	%% --- OFDM demodulator  
	% ---------------------------------------------------- 
	% From the time domain signal, we should deduce a T/F matrix 
	% of size nbSymb x nFFT 
	% Channel has to be estimated and compensated
	% ---------------------------------------------------- 
	%% --- L2 decoder  
	% ---------------------------------------------------- 
	% Decoding all usefull channel 
	% In the end, we shoulduld only have the payload of our ident 
	% ---------------------------------------------------- 
	%% --- Plotting ASCII message and CRC check  
	% ---------------------------------------------------- 
	


end


