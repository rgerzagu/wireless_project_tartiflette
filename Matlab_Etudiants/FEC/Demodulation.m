%% Démodulation pour vérification
clear all;
mcs = 16;
%--- Démodulation 16-QAM, QPSK, BPSK
load("qam16.mat");
qam16 = qam16*sqrt(2/3*15);
bitEst = bitDemapper(mcs,qam16);
nbBits = length(bS);
ber_16_qam = sum(xor(bitEst,bS))/ nbBits

mcs = 2;
load("bpsk.mat");
bitEst = bitDemapper(mcs,bpsk);
nbBits = length(bS);
ber_bpsk = sum(xor(bitEst,bS))/ nbBits


mcs = 4;
load("qpsk.mat");
bitEst = bitDemapper(mcs,qpsk);
nbBits = length(bS);
ber_qpsk = sum(xor(bitEst,bS))/ nbBits

%--- Code Hamming


