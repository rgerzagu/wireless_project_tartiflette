function [y] = encode_cc(x,config)

    %padding
    x_padding = [x zeros(1,config.tail_bit)];
    %encoding
    [codeA stateA]=convenc(x_padding,config.trellis);
    if(stateA ~= 0)
        error('Final BCC state not equal to 0\n');
    end
    
    %puncturing
    coding_rate = config.coding_rate;
    switch(coding_rate)
        case 1/5
               y = codeA;           
        case 1/3
            y = codeA;            
        case 1/2
            y = codeA;
        case 2/3
            codeAtmp = reshape(codeA,4,length(codeA)/4).';
            codeAtmp_punct = codeAtmp(:,1:3);
            y = reshape(codeAtmp_punct.',1,3*length(codeAtmp_punct(:,1)));            
        case 3/4
            codeAtmp = reshape(codeA,3,length(codeA)/3).';
            codeAtmp_punct = codeAtmp(:,1:2);
            y = reshape(codeAtmp_punct.',1,2*length(codeAtmp_punct(:,1))); 
        case 5/6
            codeAtmp = reshape(codeA,10,length(codeA)/10).';
            codeAtmp_punct = codeAtmp(:,[1 2 4 6 8 9]);
            y = reshape(codeAtmp_punct.',1,6*length(codeAtmp_punct(:,1)));             
        otherwise
            error('Coding rate %f not defined for BCC\n',coding_rate);
    end



end