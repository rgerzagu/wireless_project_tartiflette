function bitEst = bitDemapper(mcs,sigMask)
if mcs == 2
    % ---------------------------------------------------- 
    %% --- BPSK demodulator
    % ----------------------------------------------------
    bitEst = (sigMask>0);
elseif mcs == 4
    % ---------------------------------------------------- 
    %% --- Decoding QPSK 
    % ---------------------------------------------------- 
    bitEst          = zeros(length(sigMask)*log2(mcs),1);
    bitEst(1:2:end) = real(sigMask) >0;
    bitEst(2:2:end) = imag(sigMask) >0;
else
    error('Decoding not supported for other format than QPSK');
end


end

