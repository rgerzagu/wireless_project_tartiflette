function [output] = decode_cc(llr_in,config)

    % flip sign
    x_in = llr_in;
    %x_in(x_in>0.1) = 0.1;
    %x_in(x_in<-0.1) = -0.1;
    
    %de puncturing
    switch(config.coding_rate)
        case 1/5
            y = x_in;    
            depth   = 5;            
        case 1/3             
            y = x_in;    
            depth   = 3;
        case 1/2
            y = x_in;
             depth   = 2;          
        case 2/3
            codeAtmp = reshape(x_in,3,length(x_in)/3).';
            codeAtmp_punct = zeros(length( codeAtmp(:,1)),4);
            codeAtmp_punct(:,1:3) = codeAtmp;
            y = reshape(codeAtmp_punct.',1,prod(size(codeAtmp_punct)));
            depth   = 2;
        case 3/4
            codeAtmp = reshape(x_in,2,length(x_in)/2).';
            codeAtmp_punct = zeros(length( codeAtmp(:,1)),3);
            codeAtmp_punct(:,1:2) = codeAtmp;
            y = reshape(codeAtmp_punct.',1,prod(size(codeAtmp_punct))); 
                        depth   = 2;
       case 5/6     
            codeAtmp = reshape(x_in,6,length(x_in)/6).';
            codeAtmp_punct = zeros(length( codeAtmp(:,1)),10);
            codeAtmp_punct(:,[1 2 4 6 8 9]) = codeAtmp;
            y = reshape(codeAtmp_punct.',1,prod(size(codeAtmp_punct)));
                        depth   = 2;
        otherwise
            error('Coding rate %f not defined for BCC\n',coding_rate);
    end  
    
    %decoding
    DECODED = (vitdec(y,config.trellis,length(y)/depth,'term','unquant'));
    %DECODED_tmp = (vitdec(sign(y),config.trellis,length(y)/2,'term','unquant'));    
    x_est   = -2*DECODED+1;
    %x_est_tmp   = -2*DECODED_tmp+1;    
    %re encoding
    [m n] = size(x_est);
    if(m>n)
        DECODED = DECODED.';
    end
    x_encode = -(encode_cc(DECODED(1:config.K),config)*2-1);
    output.error = sum(abs(sign(x_in)-x_encode)/2);
    output.x_est = x_est;
    
    %x_encode_tmp = -(encode_cc(DECODED_tmp(1:config.K),config)*2-1);  
    
    %figure;
    %plot(x_in.*x_encode_tmp.','o'); hold on;
    %plot(x_encode_tmp.*x_encode,'sr'); grid on;
    %ylim([-1 1]);
    
    
    
end