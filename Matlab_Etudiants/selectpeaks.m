function [outputpks,outputlocs] = selectpeaks(pks, locs)
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here

distance = zeros(1,length(locs)-1);
for k=1:length(distance)
    distance(k) = locs(k+1)-locs(k);
end

if(max(distance)-min(distance)==0) % No Problemo
    outputpks = pks;
    outputlocs = locs;
    
else % Houston we have a Problem!
    temp = [0 ; pks ;0];
    
    [outputpks,  locs2] = findpeaks(temp);
    locs2 = locs2-1;
    outputlocs = locs(locs2);
end

end

