function [ signoCP ] = RemoveCP( Signal, nFFT, nCP, nbOFDMsymbols  )

%% --- Remove CP parameters ---
% -------------------------------------------------
% Signal        : 
% nFFT          :
% nCP           :
% nbOFDMsymbols :

sigCP       = reshape(Signal,nFFT+nCP,nbOFDMsymbols);
signoCP     = sigCP(1+nCP:end,:);


end

