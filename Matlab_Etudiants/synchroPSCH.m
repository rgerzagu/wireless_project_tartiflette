function [sigPSCH_freq, sigFrame,indiceDEB,Ltrame] = synchroPSCH(sigIn, nFFT, nRe, Pb, upss)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

%% Fabrication PSCH

%Monde frequentiel
sigPSCH_freq = zeros(nRe,1);

%On ne construit le PSCH que sur Nre (600) sous porteuse
k = (1:nRe);
sigPSCH_freq = 10^(Pb/20) * exp((1i * pi * upss .* k .* (k+mod(nRe,2)))/nRe) ;

figure()
grid on;
plot(real(sigPSCH_freq))
title('PSCH frequentiel');

%% On etend le PSCH au 1024 sous porteuse
sigPSCH1024 = zeros(nFFT,1);

D = [1+(1:nRe/2) nFFT-nRe/2+1:nFFT];
sigPSCH1024(D) = sigPSCH_freq;

% Monde temporel

sigPSCH1024_temp = ifft(sigPSCH1024);
figure();
plot(real(sigPSCH1024_temp));
grid on;
title('PSCH temporel');

%% Intercorrelation entre signal et notre PSCH

corrSigIdSigPSCH = xcorr(sigIn,sigPSCH1024_temp);
figure()
grid on;
plot(real(corrSigIdSigPSCH));
title('correlation entre SigIn et SigPSCH temporel');


%% Trouver max intercorrelation pour trouver debut de trame

[pks, locs] = findpeaks(real(corrSigIdSigPSCH), 'MinPeakHeight', 4); %4 car visuel

[pks, locs] = selectpeaks(pks, locs);

indiceDEB = locs(1) - length(sigIn);

%% extraction nombre de symboles

Ltrame = locs(2)-locs(1);

%% Isolation d'une trame
sigFrame = sigIn(indiceDEB:indiceDEB+Ltrame-1);

figure; plot(real(sigFrame)); grid on; title('Signal temporel qui nous interesse');

figure; plot(fftshift(abs(fft(sigFrame)))); grid on; title('Signal frequentiel qui nous interesse');
end

