function [ channel, SigFreq ] = Estim_canal( nRe, nFFT, sigFreq, sigPSCH_freq, sigSSCH_freq )

% --- Redimension de la matrice

Ind = [1+(1:nRe/2) nFFT-nRe/2+1:nFFT];

for i=1:length(Ind)
    SigFreq(i,:) = sigFreq(Ind(i),:);
end

% --- Division du PSCH par le sPSCH pour trouver le canal

channel1 = SigFreq(:,1)./sigPSCH_freq.';
channel2 = SigFreq(:,2)./sigSSCH_freq.';

channel = (channel1+channel2)/2;


end

