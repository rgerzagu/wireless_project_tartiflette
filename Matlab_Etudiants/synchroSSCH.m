function [sigSSCH_freq, sigFrame,indiceDEB,Ltrame] = synchroSSCH(sigIn, sigPSCH_freq, nFFT, nRe, Pb, upss)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
%% Fabrication SSCH

sigSSCH_freq = conj(sigPSCH_freq);

figure()
grid on;
plot(real(sigSSCH_freq))
title('SSCH frequentiel');

%% On etend le SSCH au 1024 sous porteuse
sigSSCH1024 = zeros(nFFT,1);

D = [1+(1:nRe/2) nFFT-nRe/2+1:nFFT];
sigSSCH1024(D) = sigSSCH_freq;

% Monde temporel

sigSSCH1024_temp = ifft(sigSSCH1024);
figure()
plot(real(sigSSCH1024_temp))
grid on;
title('SSCH temporel');

%% Intercorrelation entre signal et notre SSCH

corrSigIdSigSSCH = xcorr(sigIn,sigSSCH1024_temp);
figure()
grid on;
plot(real(corrSigIdSigSSCH));
title('correlation entre SigIn et SigSSCH temporel');


%% Trouver max intercorrelation pour trouver debut de trame

[pks, locs] = findpeaks(real(corrSigIdSigSSCH), 'MinPeakHeight', 4); %4 car visuel
[pks, locs] = selectpeaks(pks, locs);
indiceDEB = locs(1) - length(sigIn);

%% extraction nombre de symboles

Ltrame = locs(2)-locs(1);

%% Isolation d'une trame
sigFrame = sigIn(indiceDEB:indiceDEB+Ltrame-1);
figure; plot(real(sigFrame));
grid on;
title('Signal temporel qui nous interesse synchronise avec SSCH');

figure; plot(fftshift(abs(fft(sigFrame))));
grid on;
title('Signal frequentiel qui nous interesse synchronise avec SSCH');

end

