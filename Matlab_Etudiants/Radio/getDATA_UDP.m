function sigRx = getDATA_UDP(obj,nbPoints)
% From nbPoints complex, switch to Int8 elements 
rt = nbPoints;
nbPoints    = nbPoints * 4 * 2;         % 16 -> 64 bits (x4) + 2 paths (I and Q)
data        = recv(obj,10000);
sL          = length(data);
sV          = zeros(1,nbPoints);
cnt         = 0;
maxCnt      = max(1e4, nbPoints / sL);
flag        = false;
% --- Socket acquisition
while true
    % Get data from socket
    data = recv(obj,10000);
    % Fill the array
    sL   = length(data);
    % Check place left on buffer 
    if cnt + sL > nbPoints
        % Too many points acquired 
        occ = nbPoints - cnt;    % We converse only the beg of this buffer
    else
        % Full buffer acqusition
        occ = sL;
    end
    % Populate buffer 
    sV( cnt + (1:occ)) = data(1:occ);
    % Update pointer position
    cnt = cnt + occ;
    % Check if we shall stop
    if cnt == nbPoints || cnt > maxCnt
        break;
    end
end
% --- Data conversion
dataRx = (typecast (uint8 (sV), 'single'));
% --- Separate I and Q streams
sigRx = double(dataRx(1:2:end)) + 1i*double(dataRx(2:2:end));
if length(sigRx) ~= rt
    disp('cc');
end
end
