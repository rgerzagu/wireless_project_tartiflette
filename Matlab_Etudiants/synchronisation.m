function [sigFramePSCH] = synchronisation(sigRx, nFFT, nRe, Pb, upss)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

%% Synchronisation avec PSCH

[sigPSCH_freq, sigFramePSCH, indiceDEBPSCH,LtramePSCH] = synchroPSCH(sigRx, nFFT, nRe, Pb, upss);

%% Synchronisation avec SSCH

[sigSSCH_freq, sigFrameSSCH, indiceDEBSSCH,LtrameSSCH] = synchroSSCH(sigRx,sigPSCH_freq, nFFT, nRe, Pb, upss);

if(LtramePSCH == LtrameSSCH)
   disp('Synchonisation OK !');
   sigFramePSCH = reshape(sigFramePSCH, 14, 1096);
end
end

