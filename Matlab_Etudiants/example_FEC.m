% --- Simple example with Conv27 decoder 
close all
clear all
clc

addpath('./FEC');



% --- Create a random sequence (here loaded from Julia to check that Julia
% and Matlab versions are alignred)
bitSeq = readFloatBinary('../bitSeq.dat').';
% --- Deduce size from sequence 
nbBits = length(bitSeq);
% --- From input sequence (payload), deduce coded size 
% here, we have 1/2 with 16 bits tails (7 of v27)
nbCoded = nbBits*2+16;

% --- Create coder config 
cc = init_cc(1/2,nbCoded);
% --- Encode data 
bitCoded = encode_cc(bitSeq,cc);

% --- Load the data coded with Julia 
bitJulia = readFloatBinary('../bitCoded.dat').';
% --- We check that it is 0 --> It means that the 2 coder are the same 
sum(xor(bitJulia,bitCoded))


% --- Let's go to BPSK 
bpsk = (2*bitCoded-1);

% --- Now we decode 
% Soft decoder here, we need to populate the incoming samples in the FEC
% with LLR (log likelihood ratios) 
% We use a symbol demapper 
llr         = symbol_demapping(bpsk,ones(1,length(bpsk)),log2(2));
% Go to CC decoder 
struc    = decode_cc(llr,cc);
% --- We get bitstream and remove tails 
sEst    = struc.x_est(1:nbBits);    % This is hard symbols 
bitEst  = sEst > 0;                 % Decision 
% Calculate BER 
ber         = sum(xor(bitEst,bitSeq)) / nbBits
