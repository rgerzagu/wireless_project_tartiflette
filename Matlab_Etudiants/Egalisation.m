function [ M ] = Egalisation( SigFreq, nbOFDMsymbols, channel, eps )

% --- Division de chaque element de la matrice par le channel

M = zeros(length(SigFreq),nbOFDMsymbols);

for i=3:nbOFDMsymbols
    M(:,i) = SigFreq(:,i)./(channel+eps);
end


end

