**Objectif :** Décoder un réseau 4G LTE

**Étapes :**

*  Synchronisation de la trame OFDM
* [ ]  Déterminer le début de la trame
* [ ]  Déterminer le début de chaque symbole OFDM
* [ ]  Compenser l'offset des fréquences porteuses

*  Démodulation OFDM
(voir Matlab fait en cours)

*  Égalisation du canal
(voir Matlab fait en cours)

*  Décoder chaque transport (FEC decoder, CRC check)
* [ ]  BS control
* [ ]  User control
* [ ]  Payload

